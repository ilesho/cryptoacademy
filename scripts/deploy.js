const hre = require("hardhat");  

async function main() {
  const [deployer] = await ethers.getSigners();  
 
  const VotingPlatform = await hre.ethers.getContractFactory("VotingPlatform"); 
  const votingPlatform = await VotingPlatform.deploy(); 

  await votingPlatform.deployed();  
 
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });  
