const { expect, assert } = require("chai");
const { Signer } = require("ethers");
const { ethers } = require("hardhat")





describe("VotingPlatform", function(){

    let owner;
    let accaunt2;
    let accaunt3;

    let votingProvider; 
    beforeEach(async function(){
        [owner, accaunt2, accaunt3] = await ethers.getSigners();
        
        const VotingProvider = await ethers.getContractFactory("VotingPlatform", owner);

        votingProvider = await VotingProvider.deploy();
        
        await votingProvider.deployed(); 

    });

    it( 'correct deployed', async function(){
        expect(votingProvider.address).to.be.properAddress;
    })


    it('Owner can create vote', async function(){
        const tx  = await votingProvider.createVote([
            accaunt2.address,
            accaunt3.address
        ]); 
        
        const votesCount = await votingProvider.getVotesCount();
        
        expect(votesCount).to.eq(1) 
    })

    it('not owner can not create vote', async function(){       
        await expect( 
                votingProvider.connect(accaunt2).createVote([
                    accaunt3.address
                ])
            ).to.be.revertedWith('only owner allowed');
        const votesCount = await votingProvider.getVotesCount();
        expect(votesCount).to.eq(0) 
    })
 

 
    let sum = 10000000000000000n;
    it('Everybody can vote', async function(){
       
        await votingProvider.createVote([accaunt3.address]);  

        const tx = await votingProvider.connect(accaunt2).vote(  0,  accaunt3.address,{value:sum} )
 
        
    }) 

    it('Nobody can double vote', async function(){
       
        await votingProvider.createVote([accaunt3.address]);   

        const tx = await votingProvider.connect(accaunt2).vote(  0,  accaunt3.address,{value:sum} )
 
        await expect(votingProvider.connect(accaunt2).vote(  0,  accaunt3.address,{value:sum} ))
        .to.be.revertedWith('voted');
    }) 
 

    it('Can vote only created vote', async function(){
       
        await expect(votingProvider.connect(accaunt2).vote(  0,  accaunt2.address,{value:sum} ))
        .to.be.revertedWith('the vote is not found');
 
    })



    it('Nobody can vote after time is over', async function(){
       
        await votingProvider.createVote([accaunt3.address]);  
        await hre.ethers.provider.send('evm_increaseTime', [4 * 24 * 60 * 60]);
        await ethers.provider.send('evm_mine', []);

        await expect(votingProvider.connect(accaunt2).vote(  0,  accaunt3.address,{value:sum} ))
        .to.be.revertedWith('time is over');
    }) 



    it('Candidate must be added', async function(){
       
        await votingProvider.createVote([accaunt2.address]);  
        await hre.ethers.provider.send('evm_increaseTime', [4 * 24 * 60 * 60]);
        await ethers.provider.send('evm_mine', []);

        await expect(votingProvider.connect(accaunt2).vote(  0,  accaunt3.address,{value:sum} ))
        .to.be.revertedWith('not participate');
    }) 


    it('Candidate can be added only once', async function(){
        await expect( votingProvider.createVote([accaunt2.address, accaunt2.address]))
        .to.be.revertedWith('candidate is already in');
    }) 

    it('Must be added an candidate or candidates', async function(){
        await expect( votingProvider.createVote([]))
        .to.be.revertedWith('must be added an candidate or candidates');
    }) 




    it('need 0,01eth', async function(){
       
        await votingProvider.createVote([accaunt3.address]);   
        await hre.ethers.provider.send('evm_increaseTime', [4 * 24 * 60 * 60]);
        await ethers.provider.send('evm_mine', []);

        await expect(votingProvider.connect(accaunt2).vote(  0,  accaunt3.address,{value:sum + 1n} ))
        .to.be.revertedWith('need 0,01eth');
    }) 


    it('Owner can withdraw', async function(){
        await votingProvider.createVote([accaunt3.address]);   
        await votingProvider.withdraw(owner.address);
    })

    it('Not owner can not withdraw', async function(){
        await votingProvider.createVote([accaunt3.address]);   
        await expect(votingProvider.connect(accaunt2).withdraw(accaunt2.address))
        .to.be.revertedWith('only owner allowed');
    })



    it('Everybody can finish', async function(){
        await votingProvider.createVote([accaunt3.address]);  

        await expect(votingProvider.connect(accaunt2).finish(0))
        .to.be.revertedWith('time is not over');
    }) 


    it('Finish without candidates', async function(){
        const tx  = await votingProvider.createExpiredVote(); 
        const votesCount = await votingProvider.getVotesCount();
        
        expect(votesCount).to.eq(1) 

        await votingProvider.connect(accaunt2).finish(0)
    })

    it('Double finish', async function(){
        const tx  = await votingProvider.createExpiredVote(); 
        const votesCount = await votingProvider.getVotesCount();
        
        expect(votesCount).to.eq(1) 

        await votingProvider.connect(accaunt2).finish(0)

        await expect(votingProvider.connect(accaunt2).finish(0))
        .to.be.revertedWith('finished'); 
    })


    it('Finish with  candidates and votes', async function(){
        await votingProvider.createVote([accaunt2.address, accaunt3.address]);  
        const votesCount = await votingProvider.getVotesCount();
        expect(votesCount).to.eq(1) 

        await votingProvider.connect(accaunt2).vote(  0,  accaunt2.address,{value:sum} );

        await hre.ethers.provider.send('evm_increaseTime', [4 * 24 * 60 * 60]);
        await ethers.provider.send('evm_mine', []);

        await votingProvider.connect(accaunt2).finish(0)
    })



    it('Can finish only created vote', async function(){
       
        await expect(votingProvider.connect(accaunt2).finish(  0 ))
        .to.be.revertedWith('the vote is not found');
 
    })


    it('Finish with  candidates and votes (change order)', async function(){
        const tx  = await votingProvider.createVote([accaunt2.address, accaunt3.address]); 
        const votesCount = await votingProvider.getVotesCount();
        expect(votesCount).to.eq(1) 
 
        await votingProvider.connect(accaunt2).vote(  0,  accaunt3.address,{value:sum} );

        await hre.ethers.provider.send('evm_increaseTime', [4 * 24 * 60 * 60]);
        await ethers.provider.send('evm_mine', []);

        await votingProvider.connect(accaunt2).finish(0)
    })

    it("Can not get winner while the vote is not finished", async function(){
        const tx  = await votingProvider.createVote([accaunt2.address, accaunt3.address]);

        await expect(votingProvider.getWinner(0))
        .to.be.revertedWith('is not finished'); 
    })

    it('If two candidates have the same numbers of votes than first is winner', async function(){
        const tx  = await votingProvider.createVote([accaunt2.address, accaunt3.address]); 
        const votesCount = await votingProvider.getVotesCount();
        expect(votesCount).to.eq(1) 
 
        await votingProvider.connect(accaunt2).vote(  0,  accaunt3.address,{value:sum} );
        await votingProvider.connect(accaunt3).vote(  0,  accaunt2.address,{value:sum} );
        
        await hre.ethers.provider.send('evm_increaseTime', [4 * 24 * 60 * 60]);
        await ethers.provider.send('evm_mine', []);

        await votingProvider.connect(accaunt2).finish(0)

        const winnerAddress = await votingProvider.getWinner(0);
        expect(winnerAddress).to.eq(accaunt2.address) 
    })
     

});