// SPDX-License-Identifier: MIT


pragma solidity ^0.8.0;


///Смарт контракт подразумевает голосование за кандидатов
///комиссию сервис оставляет себе
contract VotingPlatform {

    ///владелец.
     address public owner;

    ///баланс на платформе, свободный от обязательств
     uint public balance;

    ///голосования
     Vote[] public votes;

    ///Описывает голосование
    struct Vote {
        ///считаем что голосование начинается сразу как создали
        uint createdAt;
        ///баланс на голосовании
        uint balance;
        ///отвечает на вопрос: голосование завершено?
        bool isFinished;

        //кандидаты участвующие в голосовании
        mapping ( address => Candidate) candidates;

        //адреса кандидатов для итерации
        address[] candidatesAddresses;
        
        //проголосовавшие в голосовании
        address[] voters;

        //адрес победителя
        address winner;
    }

    ///Описывает кандидата
    struct Candidate {
        ///проверка что объект инициализирован 
        bool isExists;
        ///количество голосов проголосовавших за текущего кандидата
         uint votesCount;
    } 
 


    constructor(){
        owner = msg.sender;
    }

    ///Модификатор доступа 
    modifier onlyOwner(){
        require(msg.sender == owner, "only owner allowed");
        _;
    }

    ///создание голосования
    function createVote(address[] memory candidates) external  onlyOwner returns (uint){

        require(candidates.length > 0, "must be added an candidate or candidates");

        votes.push();

        uint voteIndex = votes.length - 1;
        Vote storage newVote = votes[voteIndex];
        newVote.createdAt = block.timestamp;
        
        for(uint i = 0; i < candidates.length; i++){
            addCandidate(voteIndex, candidates[i]);
        }
        return voteIndex;
    } 

    ///Добавление кандидата к голосованию
    function addCandidate(uint voteIndex, address candidateAddress)  internal {

        ///проверяем что кандидат еще не добавлен
        require(!votes[voteIndex].candidates[candidateAddress].isExists, "candidate is already in");

        Candidate memory newCandidate = Candidate(true, 0);
        votes[voteIndex].candidates[candidateAddress] = newCandidate ;

        votes[voteIndex].candidatesAddresses.push(candidateAddress);
    }

    ///Проверяет проголосовал ли пользователь в голосовании
    function checkVoterIsVoted(uint voteIndex, address voter ) internal view returns (bool){
        address[] memory voters = votes[voteIndex].voters;
        for (uint i = 0; i < voters.length; i++) {
            if (voters[i] == voter) {
                return true;
            }
        }
        return false;
    }


    ///Голосование, доступно любому пользователю
    function vote(uint voteIndex, address candidateAddress) public payable {
        uint value = msg.value;
        require( value == 10000000000000000, "need 0,01eth");

        //проверяем что такое голосование существует
        require( (voteIndex >= 0 ) && (voteIndex < votes.length) , "the vote is not found");

        ///проверяем что кандидат добавлен
        require(votes[voteIndex].candidates[candidateAddress].isExists, "not participate");

        ///три дня время жизни голосования
        require(voteIsAlive(voteIndex), "time is over" );

        ///проверяем что в этом голосовании пользователь еще не голосовал
        require(!checkVoterIsVoted(voteIndex, msg.sender), "voted");

        //увеличиваем количество голосов проголосовавших за кандидата
        votes[voteIndex].candidates[candidateAddress].votesCount++;

        ///отмечаем пользователя как проголосовавшего в голосовании
        votes[voteIndex].voters.push(msg.sender);

        //пополняем баланс голосования
        votes[voteIndex].balance += value;
    }
 
    uint constant VOTE_LIFE_TIME = 259200;
    ///Отвечает на вопрос:
    ///Голосование еще доступно (по времени) ?
    function voteIsAlive(uint voteIndex) public view returns (bool) {
        ///три дня время жизни голосования
        return block.timestamp < ( votes[voteIndex].createdAt + VOTE_LIFE_TIME );
    }

    ///завершение голосования
    function finish(uint voteIndex) public {

        //проверяем что такое голосование существует
        require( (voteIndex >= 0 ) && (voteIndex < votes.length) , "the vote is not found");

        ///завершить после того как время закончилось
        require(!voteIsAlive(voteIndex), "time is not over" );
        require(! votes[voteIndex].isFinished, "finished");

        votes[voteIndex].isFinished = true;
 
        //если голосовантов не было (также проверяем отсутствие кандидатов, т.к. нельзя проголосовать без кандидата)
        if (votes[voteIndex].balance == 0) { 
            return;
        }

        ///забираем комиссию 
        uint commision = votes[voteIndex].balance / 10;
        votes[voteIndex].balance -= commision;
        balance += commision;

        ///определяем победителя
        ///нечеткость в ТЗ
        ///!!!когда одинаковое количество проголосовало - отдаем первому в стеке
        address winnerAddress = votes[voteIndex].candidatesAddresses[0];
        Candidate memory winner = votes[voteIndex].candidates[winnerAddress];
        for(uint i = 1; i < votes[voteIndex].candidatesAddresses.length; i++){
            address tmpAddress = votes[voteIndex].candidatesAddresses[i];
            Candidate memory tmpCandidate = votes[voteIndex].candidates[tmpAddress];
            if (tmpCandidate.votesCount > winner.votesCount){
                winnerAddress = tmpAddress;
            }
        }

        //сохраняем победителя
        votes[voteIndex].winner = winnerAddress;
        

        ///переводим деньги победителю
        address payable addresToPay = payable(votes[voteIndex].winner);
        addresToPay.transfer(votes[voteIndex].balance);  
        votes[voteIndex].balance = 0;
    }


    ///вывод денег (полученной комиссии)
    function withdraw(address payable toSend) external onlyOwner {
        toSend.transfer(balance);
        balance = 0;
    }
 
    ///отдает количество голосований
    function getVotesCount() view public returns(uint) {
        return votes.length;
    }


    ///отдает победителя
    function getWinner(uint voteIndex) view public returns(address) {
        require( votes[voteIndex].isFinished, "is not finished");
        return  votes[voteIndex].winner;
    }

 
    ///создание голосования которое уже просрочено
    ///для тестов
    function createExpiredVote() external  onlyOwner { 
        votes.push();
        Vote storage newVote = votes[votes.length - 1];
        newVote.createdAt = block.timestamp - VOTE_LIFE_TIME - 1;
    } 
}