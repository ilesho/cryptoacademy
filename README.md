фидбек после первой итерации:

1) done: Нету тасок

2) done: https://bitbucket.org/ilesho/cryptoacademy/src/1dbe33b721aaeb55f8d822c025b2eb1428668529/contracts/VotingPlatform.sol#lines-49 структура с одним полем, зачем?

3) done: https://bitbucket.org/ilesho/cryptoacademy/src/1dbe33b721aaeb55f8d822c025b2eb1428668529/contracts/VotingPlatform.sol#lines-67 кандидатов нужно закидывать сразу при создании голосования

4) done : Нету проверки на кейс, когда два кандидата набирают одинакове кол-во голосов

5) done : В тестах после evm_increaseTime нужно майнить блок


# Basic Sample Hardhat Project

This project demonstrates a basic Hardhat use case. It comes with a sample contract, a test for that contract, a sample script that deploys that contract, and an example of a task implementation, which simply lists the available accounts.

Try running some of the following tasks:

```shell
npx hardhat accounts
npx hardhat compile
npx hardhat clean
npx hardhat test
npx hardhat node
node scripts/sample-script.js
npx hardhat help
```
