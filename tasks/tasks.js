const { task } = require("hardhat/config");
require("@nomiclabs/hardhat-web3");

//пример команды на запуск
//npx hardhat appCreateVote --show-stack-traces --candidates '["0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266"]'
task("appCreateVote", "Creating a new vote")
.addParam("candidates", "an array of cadidates adresses")
.setAction(async (taskArgs)=> {
    console.log(taskArgs);
    let candidates = JSON.parse(taskArgs.candidates );
    console.log(candidates);
    const VotingProvider = await ethers.getContractFactory("VotingPlatform");
    votingProvider = await VotingProvider.deploy();
    await votingProvider.deployed(); 
    let result = await votingProvider.createVote(candidates);
    console.log('The vote was successfully created. Vote index ' + result.value );
});


//пример команды на голосование не будет, т.к. надо платить



//просто при выполнении - упадет, т.к. не создано голосование
//если создать голосование - упадет т.к. не закончилось время
//пример команды на запуск
//npx hardhat appFinishVote --show-stack-traces --index 0
task("appFinishVote", "Finish a vote")
.addParam("index", "a vote index")
.setAction(async (taskArgs)=> {
    console.log(taskArgs);
    
    const VotingProvider = await ethers.getContractFactory("VotingPlatform");
    votingProvider = await VotingProvider.deploy();
    await votingProvider.deployed();   

    await votingProvider.finish(taskArgs.index);
     
    console.log('The vote was successfully finished');
});